package logger

import (
	"fmt"
	"gitlab.com/Arash-S/go-logger/internal/compressor"
	"gitlab.com/Arash-S/go-logger/internal/helper"
	"gitlab.com/Arash-S/go-logger/internal/storage"
	"gitlab.com/Arash-S/go-logger/internal/watcher"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

type Constraint struct {
	TotalFiles int
	StoreSize  int64
	StoreTime  time.Duration
}

type IFile interface {
	Constraint(c Constraint)
	Writer() io.Writer
}

type file struct {
	path       string
	constraint Constraint
}

// File create new file logger and take log directory as argument
func File(p string) IFile {
	return &file{
		path: p,
	}
}

func (f *file) Writer() io.Writer {
	if 0 < f.constraint.TotalFiles {
		wd := watcher.Directory(f.path, f.constraint.TotalFiles)
		wd.Open()
		wd.Start(spatialRestrictionAction)
	}

	if 0 < f.constraint.StoreTime {
		wt := watcher.Timer(f.path)
		wt.Duration(f.constraint.StoreTime)
		wt.Schedule(timeRestrictionAction)
	}

	if 0 >= f.constraint.StoreSize {
		f.constraint.StoreSize = 1024 * 1024 // One GigaByte
	}

	sf := storage.File(f.path, f.constraint.StoreSize)
	sf.Open()

	return sf
}

// Constraint make new constraint
func (f *file) Constraint(c Constraint) {
	f.constraint = c
}

func spatialRestrictionAction(paths []string) {
	if 0 >= len(paths) {
		return
	}

	d, _ := filepath.Split(paths[0])
	z := compressor.Zip(d)

	defer func() {
		z.Close()

		for _, p := range paths {
			_ = os.Remove(p)
		}
	}()

	for _, p := range paths {
		z.Append(p)
	}
}

func timeRestrictionAction(path string) {
	if 0 >= len(path) {
		return
	}

	files, err := ioutil.ReadDir(path)
	if nil != err {
		helper.Fatal(err)
	}

	z := compressor.Zip(path)
	defer func() {
		z.Close()

		for _, f := range files {
			_ = os.Remove(fmt.Sprintf("%s/%s", path, f.Name()))
		}
	}()

	for _, f := range files {
		p := fmt.Sprintf("%s/%s", path, f.Name())
		z.Append(p)
	}
}
