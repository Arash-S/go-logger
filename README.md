# Go Logger

# Sample Usage
```go
package main

import (
	"gitlab.com/Arash-S/go-logger/pkg/logger"
	"log"
	"sync"
	"time"
)

var wg sync.WaitGroup

const LogDirectory = "./log"

func main() {
	lg := logger.File(LogDirectory)
	lg.Constraint(logger.Constraint{
		TotalFiles: 3,
		StoreSize: 4 * 1023, // mb
		StoreTime: time.Minute,
	})

	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetOutput(lg.Writer())

	wg.Add(2)

	go func() {
		defer wg.Done()
		for i := 0; i < 500; i++ {
			log.Println("Go Routine One: ", i)
		}
	}()

	go func() {
		defer wg.Done()
		for i := 0; i < 500; i++ {
			log.Println("Go Routine Two: ", i)
		}
	}()

	time.Sleep(2 * time.Minute)

	wg.Wait()
}
```
