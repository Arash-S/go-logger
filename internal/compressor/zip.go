package compressor

import (
	ZIP "archive/zip"
	"fmt"
	"gitlab.com/Arash-S/go-logger/internal/helper"
	"io"
	"os"
	"time"
)

type IZip interface {
	Append(p string)
	Close()
}

type zip struct {
	handler *os.File
	writer  *ZIP.Writer
}

func Zip(p string) IZip {
	f, err := os.OpenFile(fmt.Sprintf("%s/%d.zip", p, time.Now().UnixMicro()), os.O_CREATE|os.O_WRONLY, 0644)
	if nil != err {
		helper.Fatal(err)
	}

	w := ZIP.NewWriter(f)

	return &zip{
		handler: f,
		writer:  w,
	}
}

// Append takes path
func (z *zip) Append(p string) {
	f, err := os.Open(p)
	if nil != err {
		helper.Fatal(err)
	}

	defer func() {
		_ = f.Close()
	}()

	ent, err := z.writer.Create(p)
	if nil != err {
		helper.Fatal(err)
	}

	_, err = io.Copy(ent, f)
	if nil != err {
		helper.Fatal(err)
	}
}

// Close
func (z *zip) Close() {
	var err error

	err = z.writer.Close()
	if nil != err {
		helper.Fatal(err)
	}

	err = z.handler.Close()
	if nil != err {
		helper.Fatal(err)
	}
}
