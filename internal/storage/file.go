package storage

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/Arash-S/go-logger/internal/helper"
	"os"
	"runtime"
	"sync"
	"time"
)

type IFile interface {
	Open()
	Close()
	Write([]byte) (int, error)
}

type file struct {
	mutex      sync.Mutex
	handler    *os.File
	mode       os.FileMode
	flags      int
	timestamp  int64
	constraint int64 // in bytes
	path       string
}

func File(p string, s int64) IFile {
	return &file{
		mutex:      sync.Mutex{},
		flags:      os.O_CREATE | os.O_EXCL | os.O_WRONLY | os.O_APPEND,
		mode:       0755,
		path:       p,
		constraint: s,
	}
}

func (f *file) Open() {
	f.mutex.Lock()
	defer f.mutex.Unlock()

	openFile(f)
}

func (f *file) Close() {
	f.mutex.Lock()
	defer f.mutex.Unlock()

	closeFile(f)
}

func (f *file) Write(p []byte) (n int, err error) {
	f.mutex.Lock()
	defer f.mutex.Unlock()

	s, err := f.handler.Stat()

	if nil != err {
		helper.Fatal(err)
	}

	buf := make([]byte, 2048)
	runtime.Stack(buf, false)
	buf = bytes.Trim(buf, "\x00")
	msg := fmt.Sprintf("%s\n%s\n", p, buf)

	if s.Size()+int64(len(msg)) > f.constraint {
		closeFile(f)
		openFile(f)
	}

	return fmt.Fprintf(f.handler, msg)
}

func openFile(f *file) {
	if nil != f.handler {
		helper.Fatal(errors.New("Attempting to openFile a file that is currently openFile"))
	}

	f.timestamp = time.Now().UnixMicro()
	h, err := os.OpenFile(fmt.Sprintf("%s/%d", f.path, f.timestamp), f.flags, f.mode)

	if nil != err {
		helper.Fatal(err)
	}

	f.handler = h
}

func closeFile(f *file) {
	if err := f.handler.Close(); nil != err {
		helper.Fatal(err)
	}

	f.handler = nil
	f.timestamp = 0
}
