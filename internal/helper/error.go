package helper

import (
	"fmt"
	"os"
)

const exitFailure = 1

func Fatal(e error) {
	_, _ = fmt.Fprintf(os.Stderr, "%s\n", e.Error())
	os.Exit(exitFailure)
}
