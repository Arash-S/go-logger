package watcher

import (
	"time"
)

type ITimer interface {
	Schedule(f func(string))
	Duration(t time.Duration)
}

type timer struct {
	path     string
	duration time.Duration
}

func Timer(p string) ITimer {
	return &timer{
		path: p,
	}
}

// Duration set time duration
func (t *timer) Duration(d time.Duration) {
	t.duration = d
}

// Schedule scheduled execution of some function
func (t *timer) Schedule(f func(string)) {
	go func() {
		for {
			<-time.NewTimer(t.duration).C
			f(t.path)
		}
	}()
}
