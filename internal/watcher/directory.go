package watcher

import (
	"fmt"
	"gitlab.com/Arash-S/go-logger/internal/helper"
	"regexp"
	"strings"
	"syscall"
	"unsafe"
)

type IDirectory interface {
	Open()
	Close()
	Start(act func([]string))
}

type directory struct {
	path     string // Path of directory
	number   int    // How many events must occur, for the action to be called
	notifier int    // Inotify descriptor
	watcher  int    // Inotify watcher descriptor
	channel  chan []string
}

// Directory watcher, takes two parameters,
// first one is Path where files exist and
// second one is How many events should trigger the action
func Directory(p string, n int) IDirectory {
	return &directory{
		path:    p,
		number:  n,
		channel: make(chan []string),
	}
}

// Open watcher
func (f *directory) Open() {
	var err error

	if f.notifier, err = syscall.InotifyInit(); nil != err {
		helper.Fatal(err)
	}

	if f.watcher, err = syscall.InotifyAddWatch(f.notifier, f.path, syscall.IN_CREATE); nil != err {
		helper.Fatal(err)
	}
}

// Close watcher
func (f *directory) Close() {
	var err error

	if err = syscall.Close(f.watcher); nil != err {
		helper.Fatal(err)
	}

	if err = syscall.Close(f.notifier); nil != err {
		helper.Fatal(err)
	}
}

// Start watcher
func (f *directory) Start(act func([]string)) {
	go func() {
		listener(f)
	}()

	go func() {
		event(f, act)
	}()
}

// listener watcher
func listener(f *directory) {
	res := make([]string, 0)
	rgx := regexp.MustCompile("^\\d{16}$")

	for {
		buf := make([]byte, syscall.SizeofInotifyEvent*1024)
		num, err := syscall.Read(f.notifier, buf)

		if nil != err {
			helper.Fatal(err)
		} else if syscall.SizeofInotifyEvent > num {
			continue
		}

		for i := 0; i < num-syscall.SizeofInotifyEvent; {
			e := (*syscall.InotifyEvent)(unsafe.Pointer(&buf[i]))

			if 0 >= e.Mask&syscall.IN_CREATE || 0 >= e.Len {
				continue
			}

			b := (*[syscall.PathMax]byte)(unsafe.Pointer(&buf[i+syscall.SizeofInotifyEvent]))
			n := strings.TrimRight(string(b[0:e.Len]), "\000")

			if r := rgx.FindStringSubmatch(n); nil == r {
				break
			}

			res = append(res, fmt.Sprintf("%s/%s", f.path, n))
			i += syscall.SizeofInotifyEvent + int(e.Len)

			if len(res) >= f.number {
				go func(r []string) {
					f.channel <- r
				}(res[0:f.number])

				res = res[f.number:]
			}
		}
	}
}

// event action
func event(f *directory, a func([]string)) {
	for {
		select {
		case set := <-f.channel:
			a(set) // event some action
		}
	}
}
